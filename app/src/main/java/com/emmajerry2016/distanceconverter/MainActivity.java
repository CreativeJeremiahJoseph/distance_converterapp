package com.emmajerry2016.distanceconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    /*Below code is creative an xml element variables*/
    private EditText kmToMlEditText, mlToKmEditText;
    private TextView kmToMlViewText, mlToKmViewText;
    private String getKmInputedNumber, getMlInputedNumber;
    private Button convertButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /****The below code is getting the ids of km to ml in order to have access to the element field****/
        kmToMlEditText = findViewById(R.id.inputKmValue);
        kmToMlViewText = findViewById(R.id.viewKmToMlValue);

        /****The below code is getting the ids of ml to km in order to have access to the element field****/
        mlToKmEditText = findViewById(R.id.inputMilesToKm);
        mlToKmViewText = findViewById(R.id.viewMlToKmValue);

        convertButton = findViewById(R.id.convertButton);
        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //From Kilometer to miles
                getKmInputedNumber = kmToMlEditText.getText().toString();

                if (getKmInputedNumber.isEmpty()) {
                    kmToMlEditText.setError("enter a number to be converted to Ml from Km");
                }
                else {
                    //Converting String to double
                    double getKmInputedNumberToDouble = Double.parseDouble(getKmInputedNumber);
                    final double calculateFromKmToMiles = getKmInputedNumberToDouble / 1.6;
                    kmToMlViewText.setText(calculateFromKmToMiles + "ml");
                }

                          /*******************=======*********************/
                /****************************==========****************************/
                           /*******************=======*********************/
                //For Miles to Kilometer
                getMlInputedNumber = mlToKmEditText.getText().toString();
                if (getMlInputedNumber.isEmpty()) {
                    mlToKmEditText.setError("enter a number to be converted to Km from Ml");
                }
                else {
                    //Converting String to double
                    double getMlInputedNumberToDouble = Double.parseDouble(getMlInputedNumber);
                    final double calculateFromMilesToKm = getMlInputedNumberToDouble * 1.6;
                    mlToKmViewText.setText((calculateFromMilesToKm) + "km");
                }

            }
        });

    }
}